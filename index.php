<?php require_once "./code.php"; ?> 

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>s2: Repetition Control Structures and Array Manipulations</title>
	</head>
	<body>

		<h1>Repetition Control Structures</h1>

		<?php whileloop(); ?>

		<?php doWhileloop(); ?>

		<?php forLoop(); ?>

		<?php modifiedForLoop(); ?>


		<h3>Associative Array</h3>
	
			<?php echo $gradePeriods->firstGrading; ?>

		<ul>
			<?php foreach ($gradePeriods as $period => $grade) { 
				//<?= is the shortcut for <?php echo ?>
				<li>Grade in <?= $period ?> is <?= $grade ?></li>
			<?php } ?>
		</ul>


		<h3>Two-Dimensional Array</h3>
		
			<?php echo $heroes[2][1]; ?>

			<ul>

				<?php 

					foreach($heroes as $team){
						forEach($team as $member){
							?>

							<li><?php echo $member ?></li>
				<?php 

						}
					}

				?>

			</ul>

		<h3>Sorting</h3>
		
			<pre>
				<?php print_r($sortedBrands); ?>	
			</pre>

		<h3>Reversed Sorting</h3>
		
			<pre>
				<?php print_r($reverseSortedBrands); ?>	
			</pre>
			
		<h3>Array Mutations (Append)</h3>


			<h4>push</h4>
				<?php array_push($computerBrands, 'Apple') ?>

				<pre>
					<?php print_r($computerBrands); ?>	
				</pre>

			<h4>unshift</h4>
				<?php array_unshift($computerBrands, 'Dell') ?>

				<pre>
					<?php print_r($computerBrands); ?>	
				</pre>

			<h4>remove</h4>
				<?php array_pop($computerBrands); ?>

				<pre>
					<?php print_r($computerBrands); ?>	
				</pre>

			<h4>shift</h4>
				<?php array_shift($computerBrands); ?>

				<pre>
					<?php print_r($computerBrands); ?>	
				</pre>

		<h3>Count</h3>
		
			<pre>
				<?php echo count($computerBrands); ?>	
			</pre>

		<p>
			<?php echo searchBrand($computerBrands, 'HP') ?>
		</p>

			<pre>
				<?php print_r($reverseSortedBrands); ?>	
			</pre>

	<h2>Activity</h2>

		<h3>Divisibles of Five</h3>
		
			<?php activity1(); ?>

		<h3>Array Manipulation</h3>
		
			<?php array_push($students, 'John Smith') ?>

				<p>
					<?php print_r($students); ?>	
				</p>

				<p>
					<?php echo count($students);?>
				</p>

			<?php array_push($students, 'Jane Smith') ?>

				<p>
					<?php print_r($students); ?>	
				</p>

				<p>
					<?php echo count($students);?>
				</p>

			<?php array_shift($students); ?>

				<p>
					<?php print_r($students); ?>
				</p>

				<p>
					<?php echo count($students);?>
				</p>
		
	</body>
</html>